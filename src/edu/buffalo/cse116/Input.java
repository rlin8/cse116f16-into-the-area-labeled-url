package src.edu.buffalo.cse116;

import java.util.Scanner;

public class Input {
	private Display _display;
	private Game _game;
	private Scanner _scan;
	private String _tempInput;
	
	public Input(Scanner scan, Game game, Display display) {
		_game = game;
		_display = display;
		_scan = scan;
		_tempInput = "";
	}
	
	public void process(int inputSet) {
		//inputSet defines the list of acceptable input from the user
		//0 = turn start
		//1 = after move
		//2 = player sheet up - choose
		//3 = player sheet up - mark
		//4 = rebutting an accusation.
		
		String input = _scan.next();
		
		switch(inputSet) {
		case 0:
			
		break;
		
		case 1:
			
		break;
			
		case 2:
			
			if(input.equals("q")) {
				break;
			}
			else if((Integer.parseInt(input) >= 1) && (Integer.parseInt(input) >= 21)) {
				_tempInput = input;
				process(3);
			}	
		break;
			
		case 3:
			if(input.equals("q")) {
				break;
			}
			else if(input.equals("-") || input.equals("*")) {
				_game.markPlayerSheet(Integer.parseInt(_tempInput),input.charAt(0));
				//_display.playerSheet();
				//_display.playerInstructions(2);
			}
		break;
			
		case 4:
			
		break;
		}
	}
}

package src.edu.buffalo.cse116;

public class Display {
	
	public Display() {
	}
	
	/*public void playerInstructions(int instSet) {
		//instSet defines which instruction set must be displayed
		//0 = turn start
		//1 = after move
		//2 = player sheet up choose
		//3 = player sheet up mark
		//4 = rebutting an accusation
		
		switch(instSet) {
		case 0:
			
		break;
		
		case 1:
			
		break;
		
		case 2:
			System.out.println("Enter 'q' to go back to the board or the number of the mark you would like to change");
		break;
		
		case 3:
			System.out.println("Enter 'q' to go back or either '-' or '*' to change the selected mark to that value");
		break;
		
		}
		System.out.println("Press 'R' to roll");
	}*/
	
	public void accusation(String step) {
		switch(step) {
		//Ask user if they want to make an accusation or not
		case "yesOrNo":
			System.out.println("Do you want to make an accusation?");
			System.out.println("Enter yes or no: ");
		break;
		
		case "person":
			System.out.println("Who committed the crime?");
			System.out.println("Enter the person's full name: ");
		break;
		
		case "weapon":
			System.out.println("What weapon was used to commit the crime?");
			System.out.println("Enter the weapon: ");
		break;
		
		case "room":
			System.out.println("What room did the crime happen in?");
			System.out.println("Enter the room: ");
		break;
		}
	}
	
	
	public void playerSheet(char[] playerSheet) {
		System.out.println("Player sheet");
		System.out.println();
		System.out.println("People:");
		System.out.println("Mrs.White: " + playerSheet[0]);
		System.out.println("Mrs.Peacock: " + playerSheet[1]);
		System.out.println("Mrs.Scarlet: " + playerSheet[2]);
		System.out.println("Colonel Mustard: " + playerSheet[3]);
		System.out.println("Mr.Green: " + playerSheet[4]);
		System.out.println("Professor Plum: " + playerSheet[5]);
		System.out.println();
		System.out.println("Weapons:");
		System.out.println("Candlestick: " + playerSheet[6]);
		System.out.println("Revolver: " + playerSheet[7]);
		System.out.println("Rope: " + playerSheet[8]);
		System.out.println("Wrench: " + playerSheet[9]);
		System.out.println("Lead Pipe: " + playerSheet[10]);
		System.out.println("Knife: " + playerSheet[11]);
		System.out.println();
		System.out.println("Rooms:");
		System.out.println("Study: " + playerSheet[12]);
		System.out.println("Library: " + playerSheet[13]);
		System.out.println("Conservatory: " + playerSheet[14]);
		System.out.println("Hall: " + playerSheet[15]);
		System.out.println("Kitchen: " + playerSheet[16]);
		System.out.println("Ballroom: " + playerSheet[17]);
		System.out.println("Dining Room: " + playerSheet[18]);
		System.out.println("Lounge: " + playerSheet[19]);
		System.out.println("Billiard Room: " + playerSheet[20]);
	}
	
	public void board() {
		
	}
	
	public void clear() {  
		//Clears the console to eliminate clutter
		
	}
}

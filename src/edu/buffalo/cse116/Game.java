package src.edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
	private Board _board;
	private Cards _deck;
	private Dice _dice;
	private ArrayList<String> _secretCards = new ArrayList<String>();
	private ArrayList<Player> _players = new ArrayList<Player>();
	private int _currentTurn;
	private ArrayList<String> _suggestion= new ArrayList<String>();
	private ArrayList<String> _playerAnswers = new ArrayList<String>();
	//private boolean _gameRunning;
	private Scanner _scan;
	private Display _display;
	BoardGUI gui;
	ControlGUI cui;
	
	
	public Game(int numOfPlayers, Scanner scan, Display display) {
		_scan = scan;
		_display = display;
		//_gameRunning = true;
		_players = new ArrayList<Player>();
		_secretCards = new ArrayList<String>();
		_board = new Board();
		_deck = new Cards();
		_dice = new Dice();
		_secretCards = _deck.getSecretCards();
		
		//for(int i =0; i<numOfPlayers; i++) { 
		//numOfPlayers, in this case, should always be 6.
		//it seems as though this varies thoughout game play styles
		//we can discuss how we want to handle this
		
		for(int i =0; i < numOfPlayers; i++) {
			Player _player = new Player();
			_player.setCharacter(i);
			_players.add(_player);
			for(int x=0; x < (18/numOfPlayers); x++) {
				_players.get(i).addCard(_deck.getCard());
			}
		}
		_currentTurn = 1;
		gui = new BoardGUI(_players);
		//cui = new ControlGUI();
	}
	
	
	
	public Game() {
		_board = new Board();
		_deck = new Cards();
		_dice = new Dice();
		_secretCards = _deck.getSecretCards();
		for(int i = 0; i < 6; i++) {
			Player p = new Player();
			p.setCharacter(i);
			_players.add(p);
			for(int x = 0; x < 3; x++) {
				_players.get(i).addCard(_deck.getCard());
			}
		}
		_currentTurn = 1;
	}
	
///////////////////////////////////////Initialization Methods////////////////////////////////////////////////////////	
	public void setPlayerCharacter(int playerNum, int character) {
		_players.get(playerNum - 1).setCharacter(character);
	}

	
	
	
	
///////////////////////////////////////Action Methods////////////////////////////////////////////////////////		
	public void runLoop() {
		boolean gameRunning = true;
		while(gameRunning) {
			for (Player p : _players){
				//we need to figure out how this flows properly
				gui.startTurn(p);
				while (p.isRolling());
				
				gui.startMovement(p);
				while (p.isMoving()){
					gui.frame.requestFocusInWindow();
				}
				
				while (p.isSelecting()){
					if (p.isAccusing()){
						gameRunning = !accusationMode();
						p._isselecting = false;
					}
				}
				if(!gameRunning) {
					break;
				}
			}
		}
	}
	
	
	
	public boolean accusationMode(){
		//Initialization of accusation mode.
		String selection;
		boolean correctSelection = false;
		boolean hasMatchingCard = false;
		_suggestion.clear();
		
		gui.con.dropMenu.removeAllItems();
		for(String s : _deck.getAllCards()) {
			if(!_deck.getAllRoomCards().contains(s)) {
				gui.con.dropMenu.addItem(s);
			}
		}
		gui.con.dropMenuSelection = false;
		gui.con.promptClear();
		gui.con.prompt("You are making an accusation.");
		
		
		//Prompt and get person accusation
		gui.con.prompt("Select a person.");
		while(!correctSelection) {
			try
	        {Thread.sleep(0);}
			catch (Exception e)
	        {e.printStackTrace();}
			if(gui.con.dropMenuSelection) {
				selection = gui.con.dropMenu.getSelectedItem().toString();
				if(_deck.getAllPeopleCards().contains(selection)) {
					_suggestion.add(selection);
					gui.con.prompt("Person: " + selection);
					correctSelection = true;
				}
				gui.con.dropMenuSelection = false;
			}
		}
		correctSelection = false;
		
		
		//Prompt and get weapon accusation
		gui.con.prompt("Select a weapon.");
		while(!correctSelection) {
			try
	        {Thread.sleep(0);}
			catch (Exception e)
	        {e.printStackTrace();}
			if(gui.con.dropMenuSelection) {
				selection = gui.con.dropMenu.getSelectedItem().toString();
				if(_deck.getAllWeaponCards().contains(selection)) {
					_suggestion.add(selection);
					gui.con.prompt("Weapon: " + selection);
					correctSelection = true;
				}
				gui.con.dropMenuSelection = false;
			}
		}
		correctSelection = false;
		
		
		//Prompt and get room accusation
		/*gui.con.prompt("Select a room.");
		while(!correctSelection) {
			try
	        {Thread.sleep(0);}
			catch (Exception e)
	        {e.printStackTrace();}
			if(gui.con.dropMenuSelection) {
				selection = gui.con.dropMenu.getSelectedItem().toString();
				if(_deck.getAllRoomCards().contains(selection)) {
					_suggestion.add(selection);
					gui.con.prompt("Room: " + selection);
					correctSelection = true;
				}
				gui.con.dropMenuSelection = false;
			}
		}*/
		_suggestion.add(gui.con.p._current_room);
		gui.con.prompt("Room: " + _suggestion.get(2));
		
		
		
		//Check if the accusation is fully correct, if so end game right here
		if((_secretCards.get(0).equals(_suggestion.get(0))) && (_secretCards.get(1).equals(_suggestion.get(1))) && (_secretCards.get(2).equals(_suggestion.get(2)))) {
			gui.con.promptClear();
			gui.con.prompt("You won!");
			gui.con.prompt(gui.con.p.getName() + " has solved the muder mystery!");
			return true;
		}
		
		
		
		//Starting with Scarlet, go around to every player that has a card that coincides with the accusation
		//and allow them to choose from all cards they have to show
		for(Player p : _players) {
			if(p != gui.con.p) {
				gui.con.dropMenu.removeAllItems();
				for(String s : _suggestion) {
					if(p.getAllCards().contains(s)) {
						gui.con.dropMenu.addItem(s);
						gui.con.promptClear();
						gui.con.prompt(p.getName() + ", please select a card to show " + gui.con.p.getName());					
						selection = gui.con.dropMenu.getSelectedItem().toString();
					gui.con.prompt(p.getName() + " shows that they have: " + selection);
		gui.con.prompt("Your accusation is incorrect.");
		gui.con.p._isselecting = true;
		gui.con.passageBtn.setText("OK");
						//hasMatchingCard = true;
					}
				}
			/*	if(hasMatchingCard) {
					hasMatchingCard = false;
					gui.con.dropMenuSelection = false;
					
					//gui.con.prompt(gui.con.p.getName());
					while(!gui.con.dropMenuSelection) {
						try
				        {Thread.sleep(0);}
						catch (Exception e)
				        {e.printStackTrace();}
					}
					gui.con.dropMenuSelection = false;

				}*/
			}
		}
		
		while(gui.con.p._isselecting) {
			try
	        {Thread.sleep(0);}
			catch (Exception e)
	        {e.printStackTrace();}
		}
		return false;
	}
	
	
	
	
	
	public int rollDice() {
		return _dice.roll();
	}
	/*
	public void move(Player p, int x, int y, int moves) {
		p.move(x, y, moves);
	}
	*/
	public void markPlayerSheet(int changePos, char mark) {
		_players.get(_currentTurn-1).changeSheet(changePos, mark);
	}
	
	
	public boolean guess(String person, String weapon, String room) {
		//Moves accused character to room being guessed and checks to see if entire guess matches secret cards
		//Returns true if guess is correct, false if incorrect
		
		//move(person, 1, );
		if(_secretCards.contains(person) && _secretCards.contains(weapon) && _secretCards.contains(room)) {
			return true;
		}
		else {
			_playerAnswers.clear();
			ArrayList<String> guessCards = new ArrayList<String>();
			guessCards.add(person);
			guessCards.add(weapon);
			guessCards.add(room);
			//For now this will only show a randomly picked card from a player that has one of the guessed cards
			Random num = new Random();
			ArrayList<String> matchedCards = new ArrayList<String>();
			//_diceRoll = num.nextInt(6)+1;
			for(int i = 0; i<_players.size(); i++) {
				matchedCards = _players.get(i).checkCards(guessCards);
				if(matchedCards.size() != 0) {
					_playerAnswers.add(matchedCards.get(num.nextInt(matchedCards.size()-1)));
					showMatchedCards(_playerAnswers.get(_playerAnswers.size()-1));
				}
			}
			return false;
		}
	}
	
	public String showMatchedCards(String showCard) {
		//Displays a player card that proves the current guess is wrong * NOTICE* (Will need to be modified when GUI is implemented)
		return showCard;
	}
	
	public ArrayList<String> suggesion(String person, String weapon, String room){
		_suggestion.add(0, person);
		_suggestion.add(1, weapon);
		_suggestion.add(2, room);
		return _suggestion;
	}
	public void endturnif(String person, String weapon, String room,String cardhave){
		Player p= new Player();
		if(suggesion(person, weapon, room).contains(p.cardhave(cardhave))){
			endTurn();
		}
	}
	
	
	public void endTurn() {
		if(_currentTurn == _players.size()) {
			_currentTurn = 1;
		}
		else {
			_currentTurn ++;
		}
	}
	
	
	

	
	
///////////////////////////////////////Get Methods////////////////////////////////////////////////////////		
	public Player getPlayer(int playerNum) {
		return _players.get(playerNum-1);
	}
	
	public int getTurn() {
		return _currentTurn;
	}
	

	public Board getBoard(){
		return _board;
}
	public char[] getEntireSheet() {
		return _players.get(_currentTurn).getEntireSheet();
	}
	
	public char getSheetEntry(int sheetIndex) {
		return _players.get(_currentTurn).getSheetEntry(sheetIndex);
	}
	
	public ArrayList<String> getSecretCards() {
		return _secretCards;
	}
	
	public ArrayList<String> getPlayerAnswers() {
		return _playerAnswers;
	}
	
	public Cards getDeck() {
		return _deck;
	}
	
	//public boolean isRunning() {
	//	return _gameRunning;
	//}
}

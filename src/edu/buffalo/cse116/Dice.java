package src.edu.buffalo.cse116;

import java.util.Random;

public class Dice {
	protected int _diceRoll;
	
	public Dice() {
		_diceRoll = 0;
	}
	
	public int roll() {
		// I've looked through some stuff, and found that the amount of die played in Clue can vary.
		// However, it seems as though most people play with only one,
		// So I'm adjusting this class to accommodate this.
		
		//Sets and returns a random integer between (and including) 1 and 6
		Random num = new Random();
		_diceRoll = num.nextInt(6)+1;
		return _diceRoll;
		//return 10;
	}
	
	//overload for optional variable
	public int roll(int numOfDie) {
		// I've looked through some stuff, and found that the amount of die played in Clue can vary.
		// However, it seems as though most people play with only one,
		// So I'm adjusting this class to accommodate this.
		
		//Rolls 'numOfDie' amount of dice, and returns their sum
		int roll_amnt;
		if (numOfDie < 1){
			//if numOfDie is not valid, defaults to 1
			roll_amnt = 1;
		}else{
			roll_amnt = numOfDie;
		}
		
		/* 
		 * Adding die values as we go is more accurate than randomly assigning
		 * values from min to max (like 2 and 12 for two die).  Consider this case:
		 * If we wanted to roll two dice, the chances of rolling a 6 is higher than
		 * rolling a 2 (because only one combination of values, 1 and 1, add to 2, versus
		 * the number of combinations that can add to 6 (which are 3 & 3, 4 & 2, 5 & 1, etc.)
		 * This isn't the case with randomly assigning from min to max (which are evenly distributed).
		 */
		
		_diceRoll = 0;
		for (int i = 0; i < roll_amnt; ++i){
			_diceRoll += roll();
		}
		return _diceRoll;
	}
	
	
	
	public int getVal() {
		return _diceRoll;
	}
}

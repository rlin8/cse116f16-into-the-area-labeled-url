package src.edu.buffalo.cse116;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class GUI {
	public static void main(String [ ] args){

	JFrame fr3=new JFrame("guess sheet");
	fr3.setVisible(true);
	JPanel p1 = new JPanel(new GridLayout(3,7));
	final JButton b1= new JButton();
	final JButton b2= new JButton();
	final JButton b3= new JButton();
	final JButton b4= new JButton();
	final JButton b5= new JButton();
	final JButton b6= new JButton();
	final JButton b7= new JButton();
	final JButton b8= new JButton();
	final JButton b9= new JButton();
	final JButton b10= new JButton();
	final JButton b11= new JButton();
	final JButton b12= new JButton();
	final JButton b13= new JButton();
	final JButton b14= new JButton();
	final JButton b15= new JButton();
	final JButton b16= new JButton();
	final JButton b17= new JButton();
	final JButton b18= new JButton();
	final JButton b19= new JButton();
	final JButton b20= new JButton();
	final JButton b21= new JButton();
	b1.setText("Mrs.White");
	b2.setText("Mrs.Peacock");
	b3.setText("Mrs.Scarlet");
	b4.setText("Colonel Mustard");
	b5.setText("Mr.Green");
	b6.setText("Professor Plum");
	b7.setText("Candlestick");
	b8.setText("Revolver");
	b9.setText("Rope");
	b10.setText("Wrench");
	b11.setText("Lead Pipe");
	b12.setText("Knife");
	b13.setText("Study");
	b14.setText("Library");
	b15.setText("Conservatory");
	b16.setText("Hall");
	b17.setText("Kitchen");
	b18.setText("Ballroom");
	b19.setText("Dining Room");
	b20.setText("Lounge");
	b21.setText("Billiard Room");
	p1.add(b1);
	p1.add(b2);
	p1.add(b3);
	p1.add(b4);
	p1.add(b5);
	p1.add(b6);
	p1.add(b7);
	p1.add(b8);
	p1.add(b9);
	p1.add(b10);
	p1.add(b11);
	p1.add(b12);
	p1.add(b13);
	p1.add(b14);
	p1.add(b15);
	p1.add(b16);
	p1.add(b17);
	p1.add(b18);
	p1.add(b19);
	p1.add(b20);
	p1.add(b21);
	fr3.add(p1);
	b1.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b1.setBackground(Color.BLUE);
		}
	});
	b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				b2.setBackground(Color.BLUE);
			}
	});
	b3.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b3.setBackground(Color.BLUE);
		}
	});
	b4.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b4.setBackground(Color.BLUE);
		}
	});
	b5.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b5.setBackground(Color.BLUE);
		}
	});
	b6.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b6.setBackground(Color.BLUE);
		}
	});
	b7.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b7.setBackground(Color.BLUE);
		}
	});
	b8.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b8.setBackground(Color.BLUE);
		}
	});
	b9.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b9.setBackground(Color.BLUE);
		}
	});
	b10.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b10.setBackground(Color.BLUE);
		}
	});
	b11.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b11.setBackground(Color.BLUE);
		}
	});
	b12.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b12.setBackground(Color.BLUE);
		}
	});
	b13.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b13.setBackground(Color.BLUE);
		}
	});
	b14.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b14.setBackground(Color.BLUE);
		}
	});
	b15.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b15.setBackground(Color.BLUE);
		}
	});
	b16.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b16.setBackground(Color.BLUE);
		}
	});
	b17.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b17.setBackground(Color.BLUE);
		}
	});
	b18.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b18.setBackground(Color.BLUE);
		}
	});
	b19.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b19.setBackground(Color.BLUE);
		}
	});
	b20.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b20.setBackground(Color.BLUE);
		}
	});
	b21.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			b21.setBackground(Color.BLUE);
		}
	});
	fr3.setSize(800, 1200);
	
	}
}

package src.edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Random;

public class Cards {
	/*
	 * Weapons:
	 * - Candlestick
	 * - Wrench
	 * - Rope
	 * - Revolver
	 * - Knife
	 * - Lead Pipe
	 * 
	 * Rooms:
	 * - Conservatory
	 * - Lounge
	 * - Library
	 * - Kitchen
	 * - Ballroom
	 * - Hall
	 * - Billiard Room
	 * - Study
	 * - Dining Room
	 * 
	 * Characters:
	 * - Professor Plum
	 * - Mrs. White
	 * - Mr. Green
	 * - Mrs. Peacock
	 * - Miss Scarlett
	 * - Colonel Mustard
	 */
	
	/*
	 * We construct the deck from three sub-decks consisting of character cards, room cards, and weapon cards.
	 * * * *
	 */
		private ArrayList<String> _allPeople = new ArrayList<String>();
		private ArrayList<String> _allWeapons = new ArrayList<String>();
		private ArrayList<String> _allRooms = new ArrayList<String>();
		private ArrayList<String> _allCards = new ArrayList<String>();
		private ArrayList<String> _secretCards = new ArrayList<String>();
		private ArrayList<String> _deckPeople = new ArrayList<String>();
		private ArrayList<String> _deckWeapons = new ArrayList<String>();
		private ArrayList<String> _deckRooms = new ArrayList<String>();
		private ArrayList<String> _deckAll = new ArrayList<String>();
		//private ArrayList<String>[] _playerCards;
		//private String[] 

		public Cards() {	
			_allPeople.add("Mrs.White");
			_allPeople.add("Mrs.Peacock");
			_allPeople.add("Mrs.Scarlet");
			_allPeople.add("Colonel Mustard");
			_allPeople.add("Mr.Green");
			_allPeople.add("Professor Plum");
			
			_allWeapons.add("Candlestick");
			_allWeapons.add("Revolver");
			_allWeapons.add("Rope");
			_allWeapons.add("Wrench");
			_allWeapons.add("Lead Pipe");
			_allWeapons.add("Knife");
			
			_allRooms.add("Study");
			_allRooms.add("Library");
			_allRooms.add("Conservatory");
			_allRooms.add("Hall");
			_allRooms.add("Kitchen");
			_allRooms.add("Ballroom");
			_allRooms.add("Dining Room");
			_allRooms.add("Lounge");
			_allRooms.add("Billiard Room");
			
			
			for(String st : _allPeople) {
				_deckPeople.add(st);
				_deckAll.add(st);
			}
			for(String st : _allWeapons) {
				_deckWeapons.add(st);
				_deckAll.add(st);
			}
			for(String st : _allRooms) {
				_deckRooms.add(st);
				_deckAll.add(st);
			}
			
			setSecretCards();
			for(String st : _allPeople) {
				_allCards.add(st);
			}
			for(String st : _allWeapons) {
				_allCards.add(st);
			}
			for(String st : _allRooms) {
				_allCards.add(st);
			}
		}
		
		public void setSecretCards() {
			Random num = new Random();
			_secretCards.add(_allPeople.remove(num.nextInt(_allPeople.size()-1)));
			_secretCards.add(_allWeapons.remove(num.nextInt(_allWeapons.size()-1)));
			_secretCards.add(_allRooms.remove(num.nextInt(_allRooms.size()-1)));
		}
		
		public ArrayList<String> getSecretCards() {
			return _secretCards;
		}
		
		//public ArrayList<String> getPlayerCards(int playerNum) {
		//	return _playerCards[playerNum-1];
		//}
		
		public String getCard() {
			Random num = new Random();
			int index = _allCards.size()-1;
			if (_allCards.size()-1 > 0){
				index = num.nextInt(_allCards.size()-1);
			}
			return _allCards.remove(index);
		}
		
		public ArrayList<String> getAllPeopleCards() {
			return _deckPeople;
		}
		
		public ArrayList<String> getAllWeaponCards() {
			return _deckWeapons;
		}
		
		public ArrayList<String> getAllRoomCards() {
			return _deckRooms;
		}
		
		public ArrayList<String> getAllCards() {
			return _deckAll;
		}
}

package src.edu.buffalo.cse116;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.*;

public class ControlGUI extends JPanel{
	JFrame frame;
	Controls con;
	JTextArea textBox;
	JButton rollBtn;
	
	public ControlGUI(){
		frame = new JFrame();
		//con = new Controls();
		textBox = new JTextArea();
		rollBtn = new JButton();
		
		frame.setSize(600,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
	    frame.setTitle("CUI");
	    frame.add(con);
	    con.add(textBox);
	    con.add(rollBtn);
	}
}
/*
 * This is the class I used to implement the control panel.
 * ControlGUI is never used in my implementation.
 */
class Controls extends JPanel implements ActionListener{
	BoardGUI gui;
	JButton rollBtn;
	JButton passageBtn;
	JButton accuseBtn;
	JComboBox dropMenu;
	
	Font rollFont;
	Font passageFont;
	Font accuseFont;
	
	boolean dropMenuSelection = false;
	
	JPanel buttonBox = new JPanel();
	JPanel buttonBox2 = new JPanel();
	JTextArea textBox;
	Player p;
	Dice _dice;
	ArrayList<String> console = new ArrayList<String>();
	
	
	public Controls(BoardGUI _gui){
		rollFont= new Font("TimesRoman", Font.PLAIN, 40);
		passageFont= new Font("TimesRoman", Font.PLAIN, 20);
		accuseFont= new Font("TimesRoman", Font.PLAIN, 50);
		gui = _gui;
		this.setLayout(new GridLayout(4,1));
		buttonBox.setLayout(new GridLayout(1,2));
		buttonBox2.setLayout(new GridLayout(0,1));
		
		rollBtn = new JButton();
		rollBtn.setFont(rollFont);
		rollBtn.setText("Roll!");
		rollBtn.addActionListener(this);
		
		passageBtn = new JButton();
		passageBtn.setFont(passageFont);
		passageBtn.setText("Use Passage");
		passageBtn.addActionListener(this);
		
		accuseBtn = new JButton();
		accuseBtn.setFont(accuseFont);
		accuseBtn.setText("ACCUSE!");
		accuseBtn.addActionListener(this);
		
		dropMenu = new JComboBox();
		dropMenu.setEnabled(true);
		dropMenu.setEditable(false);
		dropMenu.addActionListener(this);
		
		
		textBox = new JTextArea();
		
		buttonBox.add(rollBtn);
		buttonBox.add(passageBtn);
		buttonBox2.add(accuseBtn);
		
		
		this.add(buttonBox);
		this.add(buttonBox2);
		this.add(dropMenu);
		this.add(textBox);
		
		
		
		_dice = new Dice();
		this.setFocusable(true);
	}
	
	public void setPlayer(Player p0){
		p = p0;
	}
	
	public void prompt(String text){
		console.add(text);
		String finalText = "";
		for (String s : console){
			finalText += s + "\n";
		}
		textBox.setText(finalText);
		if (console.size() > 9){
			console.remove(0);
		}
	}
	
	public void promptClear() {
		console.clear();
		textBox.setText(null);
	}
	
	@Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 650);
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
	public void paintComponent(Graphics g){
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if (src == rollBtn && p._isrolling){
			int roll = _dice.roll();
			p.setNumOfMoves(roll);
			p._isrolling = false;
			prompt("You rolled " + roll);
			rollBtn.setText(""+roll);
		}
		if (src == passageBtn && p._isrolling){
			String current = p.getCurrentRoom();
			if (current == "Study"){
				prompt("Used Passage to the Kitchen");
				p.setCurrentRoom("Kitchen");
				p.p[0] = 19;
				p.p[1] = 18;
			}
			else if (current == "Conservatory"){
				prompt("Used Passage to the Lounge");
				p.setCurrentRoom("Lounge");
				p.p[0] = 17;
				p.p[1] = 5;
			}
			else if (current == "Kitchen"){
				prompt("Used Passage to the Study");
				p.setCurrentRoom("Study");
				p.p[0] = 6;
				p.p[1] = 3;
			}
			else if (current == "Lounge"){
				prompt("Used Passage to the Conservatory");
				p.setCurrentRoom("Conservatory");
				p.p[0] = 4;
				p.p[1] = 19;
			}else{
				prompt("You must be in a connecting room.");
			}
			gui.update(gui.players);//<- notice the ugly self reference.
		}
		else if (src == passageBtn && p._isselecting) {
			p._isselecting = false;
		}
		
		
		if(src == accuseBtn && !p._current_room.equals("Board")) {
			p._isaccusing = true;
		}
		if(src == dropMenu && p._isaccusing) {
			dropMenuSelection = true;
			//System.out.println("I got here");
			//System.out.println(dropMenu.getSelectedItem());
		}
	}
}
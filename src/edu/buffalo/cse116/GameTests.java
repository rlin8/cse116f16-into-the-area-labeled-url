package src.edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

public class GameTests {
	/*
	 	* Test that your project correctly identifies as legal a move that only goes horizontally for as many squares as the die roll;
		* Test that your project correctly identifies as legal a move that only goes vertically for as many squares as the die roll;
		* Test that your project correctly identifies as legal a move that goes horizontally & vertically for as many squares as the die roll;
		* Test that your project correctly identifies as legal a move that goes through a door and into a room;
		* Test that your project correctly identifies as legal a move that just uses the a secret passageway;
		* 
		* Test that your project correctly identifies as illegal a move that goes more squares than the die roll;
		* Test that your project correctly identifies as illegal a move that goes diagonally;
		* Test that your project correctly identifies as illegal a move that is not contiguous;
		* Test that your project correctly identifies as illegal a move that goes through a wall.
	 */
	@Test
	public void movementTest1(){
		/* Test that your project correctly identifies as legal a move 
		 * that only goes horizontally for as many squares as the die roll;
		 */
		Game gm = new Game();
		int roll = 5;
		Player p = gm.getPlayer(1);
		/*
		System.out.println(p.p[0] + ", " + p.p[1]);
		System.out.println(gm.getBoard().getSpot(5,5));
		gm.move(p, p.p[0]-roll, p.p[1], roll);
		System.out.println(p.p[0] + ", " + p.p[1]);
		*/
		assertTrue(p._checkMove(p.p[0]-roll, p.p[1], roll));
		assertFalse(p._checkMove(p.p[0]+roll, p.p[1], roll));
	}
	
	@Test
	public void movementTest2(){
		/* Test that your project correctly identifies as legal a move 
		 * that only goes vertically for as many squares as the die roll;
		 */
		Game gm = new Game();
		int roll = 1;
		Player p = gm.getPlayer(1);
		
		//Manually placing the player here to test
		p.p[0] = 5;
		p.p[1] = 5;
		
		assertTrue(p._checkMove(p.p[0], p.p[1]-roll, roll));
		assertFalse(p._checkMove(p.p[0], p.p[1]+roll, roll));
	}
	
	@Test
	public void movementTest3(){
		/* Test that your project correctly identifies as legal a move 
		 * that goes horizontally & vertically for as many squares as the die roll;
		 */
		Game gm = new Game();
		int roll = 4;
		Player p = gm.getPlayer(1);
		
		//Manually placing the player here to test
		p.p[0] = 5;
		p.p[1] = 5;
		
		assertTrue(p._checkMove(p.p[0]+2, p.p[1]+2, roll));
		assertFalse(p._checkMove(p.p[0]-2, p.p[1]-2, roll));
	}
	
	@Test
	public void movementTest4(){
		/* Test that your project correctly identifies as legal a move 
		 * that goes through a door and into a room;
		 */
		Game gm = new Game();
		Player p = gm.getPlayer(1);
		
		//Manually placing the player here to test
		p.p[0] = 5;
		p.p[1] = 5;
		
		String old_room = p.getCurrentRoom();
		//p.move(6, 3, 4);
		String new_room = p.getCurrentRoom();
		assertFalse(old_room == new_room);
	}
	
	@Test
	public void movementTest5(){
		/* Test that your project correctly identifies as legal a move 
		 * that just uses the a secret passageway;
		 */
		Game gm = new Game();
		Player p = gm.getPlayer(1);
		
		//Manually placing the player here to test
		p.p[0] = 5;
		p.p[1] = 5;
		//Stepping into "Study"
		//p.move(6, 3, 4);
		String old_room = p.getCurrentRoom();
		p.usePassage();
		String new_room = p.getCurrentRoom();
		assertFalse(old_room == new_room);
	}
	
	@Test
	public void movementTest6(){
		/* Test that your project correctly identifies as illegal a move 
		 * that goes more squares than the die roll;
		 */
		Game gm = new Game();
		int roll = 1;
		Player p = gm.getPlayer(1);
		
		//Manually placing the player here to test
		p.p[0] = 5;
		p.p[1] = 5;

		assertFalse(p._checkMove(p.p[0], p.p[1]+2, roll));
	}
	
	@Test
	public void movementTest7(){
		/* Test that your project correctly identifies as illegal a move that goes diagonally;
		 * (NOTE: our game isn't setup to move diagonally at all, so this test is trivial)
		 */
		Game gm = new Game();
		int roll = 1;
		Player p = gm.getPlayer(1);
		
		//Manually placing the player here to test
		p.p[0] = 5;
		p.p[1] = 5;

		assertFalse(p._checkMove(p.p[0]+1, p.p[1]+1, roll));
	}
	/*
	* Test that your project correctly identifies as illegal a move that is not contiguous;
	* Test that your project correctly identifies as illegal a move that goes through a wall.
	* 
	* These tests can't be specifically tested in our game since movement is based on coordinates,
	* not on movements.  So there is no move that can be made that can even attempt to go through
	* a wall or be non-contiguous.  Instead, the player's move will be rejected based on if he/she
	* has rolled a high enough number (so that they can walk around a wall or to a non-contiguous spot).
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Test
	public void testplaerhaveperson(){
		// Suggestion would be answered by the next player because they have the Player card;
		
		/*return true when player.contain(person,weapon,room)=Game.guessnewxtpersonhave()*/
		/*Cards c =new Cards();
		ArrayList<String> a = c.getSecretCards();
		a.set(0, "Mrs.White");
		a.set(1, "Rope");
		System.out.println(a);
		*//*assertTrue(g.suggesion("Mrs.White", "Rope", "Hall"));*/
		Game g = new Game();
		Player p = new Player();
		assertTrue("I have the player card",g.suggesion("Mrs.White", "Rope", "Hall").contains(p.cardhave("Mrs.White")));
		}
	
	@Test
	public void testplaerhaveweapon(){
		// Suggestion would be answered by the next player because they have the Weapon card
		
		Game g = new Game();
		Player p = new Player();
		assertTrue("I have the weapon card",g.suggesion("Mrs.White", "Rope", "Hall").contains(p.cardhave("Rope")));
		}
	
	@Test
	public void testplaerhaveroom(){
		// Suggestion would be answered by the next player because they have the Room card
		
		Game g = new Game();
		Player p = new Player();
		assertTrue("I have the room card",g.suggesion("Mrs.White", "Rope", "Hall").contains(p.cardhave("Hall")));
		}
	
	@Test
	public void testplaerhaveroomandweapon(){		
		Game g = new Game();
		Player p = new Player();
		assertTrue("I have the room card",g.suggesion("Mrs.White", "Rope", "Hall").contains(p.cardhave("Hall")));
		assertTrue("I have the weapon card",g.suggesion("Mrs.White", "Rope", "Hall").contains(p.cardhave("Rope")));
		}
	
	@Test
	public void testplayerhavethecardtomakeendturn(){
		Game g=new Game();
		g.endturnif("a", "b","c","a");
		assertEquals(2,g.getTurn());
	}
	/* 
	* 
	* 
	* */
	
	/* OUTDATED vvv
	@Test
	public void testNextPlayerCardMatchGuess() {
		//Suggestion would be answered by the player after the next player because they have 1 or more matching cards;
		
		Game game = new Game(3);
		game.endTurn();
		assertFalse(game.guess(game.getPlayer(game.getTurn()+1).getAllCards().get(0),game.getPlayer(game.getTurn()+1).getAllCards().get(0),game.getPlayer(game.getTurn()+1).getAllCards().get(0)));
		assertTrue(game.getPlayerAnswers().contains(game.getPlayer(game.getTurn()+1).getAllCards().get(0)));
	}
	
	@Test
	public void testPreviousPlayerCardMatchGuess() {
		//Suggestion would be answered by the player immediately before player making suggestion because they have 1 or more matching cards;
		
		Game game = new Game(3);
		game.endTurn();
		assertFalse(game.guess(game.getPlayer(game.getTurn()-1).getAllCards().get(0),game.getPlayer(game.getTurn()-1).getAllCards().get(0),game.getPlayer(game.getTurn()-1).getAllCards().get(0)));
		assertTrue(game.getPlayerAnswers().contains(game.getPlayer(game.getTurn()-1).getAllCards().get(0)));
	}
	
	@Test
	public void testCurrentPlayerHasGuessCard() {
		//Suggestion cannot be answered by any player but the player making the suggestion has 1 or more matching cards;
		
		Game game = new Game(3);
		game.endTurn();
		assertFalse(game.guess(game.getPlayer(game.getTurn()).getAllCards().get(0),game.getPlayer(game.getTurn()).getAllCards().get(0),game.getPlayer(game.getTurn()).getAllCards().get(0)));
		assertTrue(game.getPlayerAnswers().contains(game.getPlayer(game.getTurn()).getAllCards().get(0)));
	}
	
	@Test
	public void testGuessCorrect() {
		//Suggestion cannot be answered by any player and the player making the suggestion does not have any matching cards.
		
		Game game = new Game(3);
		ArrayList<String> secretCards = game.getSecretCards();
		assertTrue(game.guess(secretCards.get(0), secretCards.get(1), secretCards.get(2)));
		assertTrue(game.getPlayerAnswers().size() == 0);
	}
	
	/*@Test
	public void testInput() {
		Scanner scan = new Scanner(System.in);
		Game game = new Game(3);
		Display display = new Display(game);
		display.playerOptions();
		System.out.println(scan.next());
		display.clear();
		System.out.println(scan.next());
		scan.close();
	}
	
	@Test public void testPlayerSheetDisplay() {
		Game game = new Game(3);
		Display display = new Display(game);
		display.playerSheet();
	}
	*/
}

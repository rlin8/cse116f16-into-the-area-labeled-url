package src.edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {
	/* Board Class
	The Board class is designed to be an all encompassing set of parameters for the game.
	Basically, by this design, everything that doesn't belong in some other class will go here,
	and the other classes will refer to the board for these parameters.
	
	In terms of the 'physical' board, I'm using
	https:s-media-cache-ak0.pinimg.com/originals/8d/4a/95/8d4a95cfddea78bcee58a7eb243ceb93.jpg
	as reference (although I think all the boards are designed the same).
	*/
	
	/* board_grid
	In order to allow things like movement or postion-based actions (such as entering a room),
	we need to know what each point of the board is (such as being an empty space or being a
	part of a room).  Below is a 24x25 grid of integers representing the board.
	At this point, this can be used for reference, but it will have to be used to implement
	mechanics into the game.  Although we are not concerned with any interface yet,
	this grid can be used to generate graphics, so it will be useful later.
	It can be printed to the console using the classes "printBoard" method.
	
	KEY:
	1: Movable Space
	2: Door
	3: Room
	4: Passage
	0: Non-Valid Space
	*/
	
	// constructor
	ArrayList<String> envelope;
	List<Player> players = new ArrayList<Player>();
	private ArrayList<String> _characterMapping; //Will be used to convert character name to move from a string to the correct coordinates on _positions
	ArrayList<int[]> _positions = new ArrayList<int[]>(); //filled with {x,y}
	//Cards deck = new Cards();
	
	public Board(){
		//Initialize all instance variables
		_characterMapping = new ArrayList<String>();
		_characterMapping.add("Mrs.White");
		_characterMapping.add("Mrs.Peacock");
		_characterMapping.add("Mrs.Scarlet");
		_characterMapping.add("Colonel Mustard");
		_characterMapping.add("Mr.Green");
		_characterMapping.add("Professor Plum");
	}
	
	int[][] board_grid = {
			{3,3,3,3,3,3,0,		1,0,	3,3,3,3,3,3,	0,1,	0,3,3,3,3,3,3}, //0
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, 
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{4,3,3,3,3,3,2,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	2,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, //4
			{1,1,1,1,1,1,1,		1,1,	3,3,3,3,3,3,	1,1,	2,3,3,3,3,3,4},
			
			{0,3,3,3,3,3,1,		1,1,	3,3,2,2,3,3,	1,1,	1,1,1,1,1,1,0}, //6
			{3,3,3,3,3,3,3,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,1},
			{3,3,3,3,3,3,2,		1,1,	0,0,0,0,0,1,	1,1,	1,1,1,1,1,1,0},
			{3,3,3,3,3,3,3,		1,1,	0,0,0,0,0,1,	1,3,	2,3,3,3,3,3,3},
			{0,3,3,2,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3}, //11
			
			{3,2,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,2,	3,3,3,3,3,3,3}, //12
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,2,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,0},
			
			{0,1,1,1,1,1,1,		1,3,	2,3,3,3,3,2,	3,1,	1,1,1,1,1,1,1}, //17
			{1,1,1,1,1,1,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,2,3,3,3,0},
			
			{0,4,3,3,2,1,1,		1,2,	3,3,3,3,3,3,	2,1,	1,3,3,3,3,3,3}, //19
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,0,		1,1,	1,3,3,3,3,1,	1,1,	0,4,3,3,3,3,3},
			{0,0,0,0,0,0,0,		0,0,	1,0,0,0,0,1,	0,0,	0,0,0,0,0,0,0}
		//   0                  7       9               15      17	
	};
	
	public int getSpot(int x, int y){
		return board_grid[x][y];
	}
	
	
	public void printBoard(){
		int point;
		for (int y = 0; y < 25; ++y){
			for (int x = 0; x < 24; ++x){
				point = this.board_grid[y][x];
				/*
				for (Player p : this.players){
					if (p.p[0] == x && p.p[1] == y){
						System.out.print(p.col+"|");
						point = 5;
					}
				}
				*/
				if (point == 4){
					System.out.print("?|");
				}
				if (point == 3){
					System.out.print("#|");
				}
				if (point == 2){
					System.out.print("/|");
				}
				if (point == 1){
					System.out.print("_|");
				}
				if (point == 0){
					System.out.print("  ");
				}
				
			}
			System.out.print("\n");
		}
	}
	
}

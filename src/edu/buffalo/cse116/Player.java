package src.edu.buffalo.cse116;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Player {
	int id;
	//String name;
	Color color;
	char col; //alternative to color
	int[] p = {0,0};
	int dx, dy;
	
	private ArrayList<String> _cards;
	private String _name;
	private char[] _guessSheetPeople;
	private char[] _guessSheetWeapons;
	private char[] _guessSheetRooms;
	private boolean _isHuman;
	String _current_room = "Board";
	private char[] _guessSheet;
	//private boolean _isHuman;
	
	//GUI Stuff
	int numOfMoves;
	ArrayList<Integer> steppedTiles = new ArrayList<Integer>();
	// Allow for the loops in the main game loop
	volatile boolean _ismoving = false;
	volatile boolean _isrolling = false;
	volatile boolean _isaccusing = false;
	public boolean _isselecting = false;

	
	public Player() {
		_cards = new ArrayList<String>();
		_guessSheet = new char[21];
		for(int i=0; i<_guessSheet.length; i++) {
			_guessSheet[i] = '-';
		}
	}
	
	public void setCharacter(int id){
		switch(id){
		
		case 0: _name = "Miss Scarlet";
		color = Color.red;
		col = 'r';
		p[0] = 16;
		p[1] = 0;
		break;
		
		case 5: _name = "Colonel Mustard";
		color = Color.yellow;
		col = 'y';
		p[0] = 23;
		p[1] = 7;
		break;
		
		case 1: _name = "Professor Plum";
		color = Color.pink;
		col = 'p';
		p[0] = 0;
		p[1] = 5;
		break;
		
		case 2: _name = "Mr. Green";
		color = Color.green;
		col = 'g';
		p[0] = 9;
		p[1] = 24;
		break;
		
		case 3: _name = "Mrs. White";
		color = Color.white;
		col = 'w';
		p[0] = 14;
		p[1] = 24;
		break;
	
		case 4: _name = "Mrs. Peacock";
		color = Color.blue;
		col = 'b';
		p[0] = 0;
		p[1] = 18;
		break;
		}
	}
	
	public void addCard(String addedCard) {
		_cards.add(addedCard);
	}
	
	public ArrayList<String> getAllCards() {
		return _cards;
	}
	
	public ArrayList<String> checkCards(ArrayList<String> checkCards) {
		//Checks for any matches between cards passed in and the current cards the player holds.
		//Returns all matching cards
		ArrayList<String> matches = new ArrayList<String>();
		for(String c : checkCards) {
			if(_cards.contains(c)) {
				matches.add(c);
			}
		}
		return matches;
	}
	
	public String cardhave(String addedcards){
		return addedcards;
	}
	public void changeSheet(int changeIndex, char mark){
		_guessSheet[changeIndex] = mark;
	}
	
	public char[] getEntireSheet() {
		return _guessSheet;
	}
	
	public char getSheetEntry(int sheetIndex) {
		return _guessSheet[sheetIndex];
	}
	
	public String getName(){
		return _name;
	}
	
	//GUI STUFF
	
	public void setNumOfMoves(int roll){
		System.out.println("Rolled " + roll);
		numOfMoves = roll+1;
	}
	
	public void startMovement(){
		steppedTiles = new ArrayList<Integer>();
		_ismoving = true;
	}
	
	public void endMovement(){
		_ismoving = false;
	}
	
	public Boolean isMoving(){
		return _ismoving;
	}
	
	public Boolean isRolling(){
		return _isrolling;
	}
	
	public Boolean isAccusing(){
		return _isaccusing;
	}
	
	public Boolean isSelecting(){
		return _isselecting;
	}
	
	//OUTDATED vvv
	
	public void _moveTo(int x, int y){
		//Moves the player to the position (x,y)
		p[0] = x;
		p[1] = y;
	}
	
	public boolean _checkMove(int x, int y, int moves){
		// Returns true if character can move to the spot, False if else
		
		if (x < 0 || x > 23 || y < 0 || y > 24){
			// if coordinate is outside of bounds
			return false;
		}
		if (Math.abs(p[0]-x)+Math.abs(p[1]-y) > moves){
			// if we don't have enough moves
			return false;
		}
		Board tmp = new Board();
		int spot_type = tmp.getSpot(y, x); //notice! inverted coordinates;
		if (spot_type != 1 && spot_type != 2){
			return false;
		}
		return true;
	}
	
	
	public void setCurrentRoom(String room){
		_current_room = room;
	}
	public String getCurrentRoom(){
		return _current_room;
	}
	/*OUTDATED
	public void move(int x, int y, int moves){
		if (_checkMove(x, y, moves)){
			_moveTo(x,y);
			_current_room = getRoom(x,y);
		}
	}*/
	
	public void usePassage(){
		//Uses a players turn to jump through a passage if available
		String current = getCurrentRoom();
		if (current == "Study"){
			_current_room = "Kitchen";
			p[0] = 19;
			p[1] = 18;
		}
		if (current == "Conservatory"){
			_current_room = "Lounge";
			p[0] = 17;
			p[1] = 5;
		}
		if (current == "Kitchen"){
			_current_room = "Study";
			p[0] = 6;
			p[1] = 3;
		}
		if (current == "Lounge"){
			_current_room = "Conservatory";
			p[0] = 4;
			p[1] = 18;
		}
	}

}

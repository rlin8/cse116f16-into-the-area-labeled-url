package src.edu.buffalo.cse116;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.*;

public class BoardGUI extends JPanel{
	ArrayList<Player> players;
	Player current_player;
	JFrame frame;
	JPanel mainPanel;
	Blocks board;
	Controls con;
	int[][] grid = {
			{3,3,3,3,3,3,0,		1,0,	3,3,3,3,3,3,	0,1,	0,3,3,3,3,3,3}, //0
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, 
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{4,3,3,3,3,3,2,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	2,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, //4
			{1,1,1,1,1,1,1,		1,1,	3,3,3,3,3,3,	1,1,	2,3,3,3,3,3,4},
			
			{0,3,3,3,3,3,1,		1,1,	3,3,2,2,3,3,	1,1,	1,1,1,1,1,1,0}, //6
			{3,3,3,3,3,3,3,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,1},
			{3,3,3,3,3,3,2,		1,1,	0,0,0,0,0,1,	1,1,	1,1,1,1,1,1,0},
			{3,3,3,3,3,3,3,		1,1,	0,0,0,0,0,1,	1,3,	2,3,3,3,3,3,3},
			{0,3,3,2,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3}, //11
			
			{3,2,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,2,	3,3,3,3,3,3,3}, //12
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,2,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,0},
			
			{0,1,1,1,1,1,1,		1,3,	2,3,3,3,3,2,	3,1,	1,1,1,1,1,1,1}, //17
			{1,1,1,1,1,1,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,2,3,3,3,0},
			
			{0,4,3,3,2,1,1,		1,2,	3,3,3,3,3,3,	2,1,	1,3,3,3,3,3,3}, //19
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,0,		1,1,	1,3,3,3,3,1,	1,1,	0,4,3,3,3,3,3},
			{0,0,0,0,0,0,0,		0,0,	1,0,0,0,0,1,	0,0,	0,0,0,0,0,0,0}
		//   0                  7       9               15      17	
	};
	
	public BoardGUI(ArrayList<Player> _players){
		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setFocusable(true);
		players = _players;
		frame = new JFrame("Key Listener");
		frame.setFocusable(true);
		board = new Blocks(_players);
		con = new Controls(this);
		
		
		frame.setLayout(new GridBagLayout());

		frame.setLocation(50,50);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.add(mainPanel);
		mainPanel.add(board);
		mainPanel.add(con);
		frame.pack();
	    frame.setVisible(true);
	}
	
	public void startTurn(final Player p0){
		p0._isrolling = true;
		con.setPlayer(p0);
		con.promptClear();
		con.prompt("It's " + p0.getName() +"'s turn.  Please Roll.");
		con.rollBtn.setText("Roll!");
		con.passageBtn.setText("Use Passage");
		con.dropMenu.removeAllItems();
		for(String s : p0.getAllCards()) {
			con.dropMenu.addItem(s);
		}
	}
		
	public void startMovement(final Player p0){
		p0._ismoving = true;
		p0.steppedTiles = new ArrayList<Integer>();
		p0.steppedTiles.add(encodeBlock(p0.p[0], p0.p[1]));
		board.stepOn(p0.p[0], p0.p[1]);
		con.passageBtn.setText("");

		KeyListener listener = new KeyListener(){

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				int[] tmp = {p0.p[0], p0.p[1]};

				if (key == KeyEvent.VK_LEFT) {
		            tmp[0] += -1;
		        }

		        if (key == KeyEvent.VK_RIGHT) {
		        	tmp[0] += 1;
		        }

		        if (key == KeyEvent.VK_UP) {
		        	tmp[1] += -1;
		        }

		        if (key == KeyEvent.VK_DOWN) {
		        	tmp[1] += 1;
		        }
		        if (tmp[0] >= 0 && tmp[0] < grid[0].length && tmp[1] >= 0 && tmp[1] < grid.length){
		        	if (grid[tmp[1]][tmp[0]] == 1){
		        		//if the player wants to walk on a tile, do this...
		        		p0.setCurrentRoom("Board");
		        		int encoded_block = encodeBlock(tmp[0],tmp[1]);
		        		
		        		if (p0.steppedTiles.contains(encoded_block)){
		        			int ind0 = p0.steppedTiles.indexOf(encoded_block);
		        			ArrayList<Integer> newTiles = new ArrayList<Integer>();
		        			for (int i = 0; i < ind0; ++i){
		        				newTiles.add(p0.steppedTiles.get(i));
		        			}
		        			p0.steppedTiles = newTiles;
		        			board.resetGrid();
		        			int[] dec;
		        			for (int z : newTiles){
		        				dec = decodeBlock(z);
		        				board.stepOn(dec[0], dec[1]);
		        			}
		        		}
		        		
		        		p0.steppedTiles.add(encoded_block);
		        		board.stepOn(tmp[0],tmp[1]);
		        		
		        		//System.out.println(p0.steppedTiles);
				        p0.p = tmp;
				        con.rollBtn.setText(""+(p0.numOfMoves-p0.steppedTiles.size()));
		        	}
		        	//if a player walks into a door...
		        	if (grid[tmp[1]][tmp[0]] == 2){
		        		String room = getRoom(tmp[0], tmp[1]);
		        		p0.setCurrentRoom(room);
		        		System.out.println("Entered Room: " + room);
		        		con.prompt("You've entered the "+room);
		        		p0._isselecting = true;
		        		p0.endMovement();
			        	frame.removeKeyListener(this);
			        	board.resetGrid();
			        	p0.p = tmp;
			        	con.rollBtn.setText(""+(p0.numOfMoves-p0.steppedTiles.size()));
			        	con.accuseBtn.setText("ACCUSE!");
			        	con.passageBtn.setText("No");
			        	con.prompt("Would you like to make an accusation?");
		        	}
		        }
		        
		        if (p0.numOfMoves <= p0.steppedTiles.size()){
		        	p0.endMovement();
		        	frame.removeKeyListener(this);
		        	board.resetGrid();
		        }
		        update(players);
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
			
		};
		
	    update(players);
	    frame.addKeyListener(listener);
	    frame.requestFocusInWindow();
	}
	
	public int encodeBlock(int x, int y){
		return x+y*24;
	}
	
	public int[] decodeBlock(int enc){
		int tmp = enc;
		int factor = 0;
		while (tmp >= 0){
			tmp = tmp - 24;
			factor = factor + 1;
		}
		tmp = tmp+24;
		factor = factor - 1;
		int[] ret = {tmp, factor};
		return ret;
	}
	
	public void addNotify() {
        super.addNotify();
        requestFocus();
    }
	
	public void update(ArrayList<Player> _players){
		players = _players;
		board.setPlayers(players);
		board.revalidate();
		board.repaint();
	}
	
	public String getRoom(int x, int y){
		// returns the name of the room the player is moving to ("Board" if not a room)
		
		if ( (x == 6 && y == 3) ){
			return "Study";
		}
		
		if (	(x == 9 && y == 4) ||
				(x == 11 && y == 6) ||
				(x == 12 && y == 6)	){
			return "Hall";
		}
		
		if ( (x == 17 && y == 5) ){
			return "Lounge";
		}
		
		if (	(x == 6 && y == 8) ||
				(x == 3 && y == 10)){
			return "Library";
		}
		
		if (	(x == 1 && y == 12) ||
				(x == 5 && y == 15)){
			return "Billard Room";
		}
		
		if (	(x == 17 && y == 9) ||
				(x == 16 && y == 12)){
			return "Dining Room";
		}
		
		if (	(x == 4 && y == 19) ){
			return "Conservatory";
		}
		
		if (	(x == 8 && y == 19) ||
				(x == 9 && y == 17) ||
				(x == 14 && y == 17)||
				(x == 15 && y == 19)){
			return "Ballroom";
		}
		
		if (	(x == 19 && y == 18) ){
			return "Kitchen";
		}
		
		return "Board";
	}
}

class Blocks extends JPanel{
	int s = 25; //size of block
	ArrayList<Player> players;
	int[][] grid = {
			{3,3,3,3,3,3,0,		1,0,	3,3,3,3,3,3,	0,1,	0,3,3,3,3,3,3}, //0
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, 
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{4,3,3,3,3,3,2,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	2,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, //4
			{1,1,1,1,1,1,1,		1,1,	3,3,3,3,3,3,	1,1,	2,3,3,3,3,3,4},
			
			{0,3,3,3,3,3,1,		1,1,	3,3,2,2,3,3,	1,1,	1,1,1,1,1,1,0}, //6
			{3,3,3,3,3,3,3,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,1},
			{3,3,3,3,3,3,2,		1,1,	0,0,0,0,0,1,	1,1,	1,1,1,1,1,1,0},
			{3,3,3,3,3,3,3,		1,1,	0,0,0,0,0,1,	1,3,	2,3,3,3,3,3,3},
			{0,3,3,2,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3}, //11
			
			{3,2,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,2,	3,3,3,3,3,3,3}, //12
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,2,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,0},
			
			{0,1,1,1,1,1,1,		1,3,	2,3,3,3,3,2,	3,1,	1,1,1,1,1,1,1}, //17
			{1,1,1,1,1,1,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,2,3,3,3,0},
			
			{0,4,3,3,2,1,1,		1,2,	3,3,3,3,3,3,	2,1,	1,3,3,3,3,3,3}, //19
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,0,		1,1,	1,3,3,3,3,1,	1,1,	0,4,3,3,3,3,3},
			{0,0,0,0,0,0,0,		0,0,	1,0,0,0,0,1,	0,0,	0,0,0,0,0,0,0}
		//   0                  7       9               15      17	
	};
	
	int[][] grid_hardcopy = {
			{3,3,3,3,3,3,0,		1,0,	3,3,3,3,3,3,	0,1,	0,3,3,3,3,3,3}, //0
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, 
			{3,3,3,3,3,3,3,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			{4,3,3,3,3,3,2,		1,1,	3,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	2,3,3,3,3,3,	1,1,	3,3,3,3,3,3,3}, //4
			{1,1,1,1,1,1,1,		1,1,	3,3,3,3,3,3,	1,1,	2,3,3,3,3,3,4},
			
			{0,3,3,3,3,3,1,		1,1,	3,3,2,2,3,3,	1,1,	1,1,1,1,1,1,0}, //6
			{3,3,3,3,3,3,3,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,1},
			{3,3,3,3,3,3,2,		1,1,	0,0,0,0,0,1,	1,1,	1,1,1,1,1,1,0},
			{3,3,3,3,3,3,3,		1,1,	0,0,0,0,0,1,	1,3,	2,3,3,3,3,3,3},
			{0,3,3,2,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			
			{0,1,1,1,1,1,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3}, //11
			
			{3,2,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,2,	3,3,3,3,3,3,3}, //12
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	0,0,0,0,0,1,	1,3,	3,3,3,3,3,3,3},
			{3,3,3,3,3,2,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,1,	1,1,1,1,1,1,	1,1,	1,1,1,1,1,1,0},
			
			{0,1,1,1,1,1,1,		1,3,	2,3,3,3,3,2,	3,1,	1,1,1,1,1,1,1}, //17
			{1,1,1,1,1,1,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,2,3,3,3,0},
			
			{0,4,3,3,2,1,1,		1,2,	3,3,3,3,3,3,	2,1,	1,3,3,3,3,3,3}, //19
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,1,		1,3,	3,3,3,3,3,3,	3,1,	1,3,3,3,3,3,3},
			{3,3,3,3,3,3,0,		1,1,	1,3,3,3,3,1,	1,1,	0,4,3,3,3,3,3},
			{0,0,0,0,0,0,0,		0,0,	1,0,0,0,0,1,	0,0,	0,0,0,0,0,0,0}
		//   0                  7       9               15      17	
	};
	
	public Blocks(ArrayList<Player> _players){
		this.setLayout(new GridLayout());
		players = _players;
		this.setFocusable(true);
	}
	
	@Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 650);
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
	
	public void resetGrid(){
		for (int y = 0; y < grid.length; y++){
			for (int x = 0; x < grid[y].length; x++){
				grid[y][x] = grid_hardcopy[y][x];
			}
		}
	}
	public void stepOn(int x, int y){
		grid[y][x] = 5;
	}
	public void stepOff(int x, int y){
		grid[y][x] = 1;
	}
	public Color setBlockColor(int t){
		if (t == 0){
			//void
			return Color.black;
		}
		if (t == 1){
			//Hall
			return Color.white;
		}
		if (t == 2){
			//Door
			return Color.red;
		}
		if (t == 3){
			//Room
			return Color.blue;
		}
		if (t == 4){
			return Color.cyan;
		}
		if (t == 5){
			return Color.gray;
		}
		return Color.black;
	};
    public void paintComponent(Graphics g){
    	Color tile_color;
        for (int y = 0; y < grid.length; ++y){
        	for (int x = 0; x < grid[y].length; ++x){
        		tile_color = setBlockColor(grid[y][x]);
        		
	        	g.setColor(tile_color);
	        	g.fillRect(s*x,s*y,s,s);
	        	g.setColor(Color.black);
	        	g.drawRect(s*x,s*y,s,s);
        	}
        }
        
        for (Player p : players){
        	g.setColor(p.color);
        	g.fillOval(p.p[0]*s, p.p[1]*s, s, s);
        	g.setColor(Color.black);
        	g.drawOval(p.p[0]*s, p.p[1]*s, s, s);
        }
    }

    public void setPlayers(ArrayList<Player> _players){
    	players = _players;
    }
}
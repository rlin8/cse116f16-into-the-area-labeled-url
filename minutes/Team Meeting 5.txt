Meeting Minutes for Oct. 25th:

* Meeting Attendance: (list all students who were present) [1 minute]
    * Nathan Margaglio (M)
	* Joshua Drummer (U)
	* RenJie Lin
* User stories completed since last meeting [1-2 minutes]
    * Getting the GUI to display
* User stories worked upon but not completed since last meeting [2-4 minutes]
    * Movement in the GUI
    * Accusation/Guessing
* Goals for the next meeting [20+ minutes]
    * Still working on movement
    * Still working through accusation/guessing
* Schedule for the next week's set of pair programming meetings [3-4 minutes]
    * Setting up the main game loop between the pieces of code we individually worked on

Meeting Minutes for Nov. 1st:

* Meeting Attendance: (list all students who were present) [1 minute]
    * Nathan Margaglio (M)
	* Joshua Drummer (U)
	* RenJie Lin
* User stories completed since last meeting [1-2 minutes]
    * As a player, I want turns to follow the order that Miss Scarlet goes first (this is from the rules), then Professor Plum, Mr. Green, Mrs. White, Mrs. Peacock, and, finally, Col. Mustard so we can have a simple ordering.
    * As a player, I want to be able to see the board and where each player is on the board so we can see where each player's piece is.
* User stories worked upon but not completed since last meeting [2-4 minutes]
    * As a player, I want to know whose turn it is and the result of the die roll (e.g., a random integer between 1 and 6) so we can know how many square they may move.
    * As a player, I want to be able to see which cards the player whose turn it is has in their hand so they can decide where to go.
    * As a player, I want to be able to input my move into the system and only have legal moves accepted so that we know everyone is following the rules.
* Goals for the next meeting [20+ minutes]
    * Combining back end stuff with the GUI
* Schedule for the next week's set of pair programming meetings [3-4 minutes]
    * Designing the GUI to be up to par

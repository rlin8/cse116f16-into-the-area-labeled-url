User Story 2:
*	As a player, I want to be able to see the board and where each player is on the 
board so we can see where each player's piece is.

How to observe functionality:
-Start game (by running program)
-Observe board and all colored circles representing each player and their current 
position on the board


User Story 5:
*	As a player, I want to be able to input my move into the system and only have 
legal moves accepted so that we know everyone is following the rules.

How to observe functionality:
-Start game (by running program)
-Click the roll button
-Press any of the arrow keys to move the current character
-Observe the fact that characters cannot move through walls, only through doors (red 
squares), and only use passages when in a room (by clicking the Use Passage button)


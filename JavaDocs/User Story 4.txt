User Story 4:
*	As a player, I want to be able to see which cards the player whose turn it is has 
in their hand so they can decide where to go.

How to observe functionality:
-Start game (by running program)
-Observe text output in game window to see whose turn it currently is
-Click the drop down menu
-Observe all values in the drop down menu to see all cards that the current player 
holds in their hand

